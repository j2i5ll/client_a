package com.ysj.client_a.msg;

import com.ysj.client_a.js.WebJSCaller;

import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.webkit.WebView;
import client_a_service.msg.common.MessageStoAConstant;



//activity MessageManager
public class MessageManager extends Handler{
	private WebJSCaller webJS=null;
	private static final String TAG = "MessageReceiver";
	
	private Messenger serviceMessenger;
	
	private static MessageManager instance=null;
	
	public static MessageManager getInstance(){
		if(instance == null){
			instance = new MessageManager();
		}
		return instance;
	}
	public void init(WebView webView){
		webJS = new WebJSCaller(webView);
	}
	private MessageManager(){}
	
	public void setServiceMessenger(Messenger serviceMessenger){
		this.serviceMessenger = serviceMessenger;
	}
	public Messenger getServiceMessenger(){
		return this.serviceMessenger;
	}
	@Override
	public void handleMessage(Message msg){
		Log.d(TAG, "RECV : "+msg.what);
		switch(msg.what){
			case MessageStoAConstant.MSG_STATE_UPDATE :
				webJS.stateUpdate();
				break;
			default:
				super.handleMessage(msg);
				break;
				
		}
	}
	
	public void send(Message msg){
		if(serviceMessenger != null){
			try {
				this.serviceMessenger.send(msg);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}
	
	
}
