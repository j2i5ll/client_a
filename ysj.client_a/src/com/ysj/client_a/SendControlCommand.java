package com.ysj.client_a;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.Socket;


import android.os.AsyncTask;
import android.widget.Toast;

public class SendControlCommand extends AsyncTask<String, Integer, Integer>{
	protected final String PREFIX_CONTROL="control";
	protected final String PREFIX_SHUTDOWN="shutdown";
	protected final String PREFIX_SCHEDULE="schlist";
	
	public static final int TRANSFER_COMPLETE = 0;
	public static final int CONNECTION_ERROR = 1;
	public static final int ETC_ERROR = 2;
	public static final int TRANSFER_COMPLETE_NO_MSG = 3;
	
	private CommunicationActivity targetActivity;
	
	public SendControlCommand(CommunicationActivity a){
		this.targetActivity=a;
	}
	@Override
	protected Integer doInBackground(String... params) {
		// TODO Auto-generated method stub
		int returnVal=0;
		String message = params[0]+"@";
		message += params[1];
		Socket soc=null;
		PrintWriter out=null;
		BufferedReader in=null;
		try {
			soc = new Socket("", 30501);
			soc.setSoTimeout(2500);
			
			out = new PrintWriter ( new BufferedWriter (new OutputStreamWriter(soc.getOutputStream())));
			in = new BufferedReader(new InputStreamReader(soc.getInputStream()));
			out.print(message);
			out.flush();
			
			returnVal = this.targetActivity.onResponseProcess(in);
						
		}catch(ConnectException e){
			returnVal=CONNECTION_ERROR;
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			returnVal=ETC_ERROR;
		
		}finally{
			try {
				if(out!=null)out.close();
				if(in!=null)in.close();
				if(soc!=null)soc.close();
			
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return returnVal;
	}
	@Override
	protected void onPostExecute(Integer result) {
		// TODO Auto-generated method stub
		if(result== TRANSFER_COMPLETE){
			Toast.makeText(targetActivity, "Transfer Complete..", Toast.LENGTH_LONG).show();
		}else if(result == TRANSFER_COMPLETE_NO_MSG){
			
		}else if(result == CONNECTION_ERROR){
			Toast.makeText(targetActivity, "Cannot connect to server..", Toast.LENGTH_LONG).show();
		}else if(result == ETC_ERROR){
			Toast.makeText(targetActivity, "Exception Occured..", Toast.LENGTH_LONG).show();
		}
	}
	
	
	//command list
	public void volumeUp(){	this.execute(PREFIX_CONTROL, "up");	}
	public void volumeDown(){	this.execute(PREFIX_CONTROL, "down");	}
	public void next(){	this.execute(PREFIX_CONTROL, "right");	}
	public void prev(){	this.execute(PREFIX_CONTROL, "left");	}
	public void pause(){	this.execute(PREFIX_CONTROL, "center");	}
	public void getScheduleList(){	this.execute(PREFIX_SCHEDULE, "schlist");	}
	public void shutdown(String time){	this.execute(PREFIX_SHUTDOWN, time);	}
	

}