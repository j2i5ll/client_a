package com.ysj.client_a;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Paint.Style;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;

public class ClientAView extends View{
	private Paint paint;
	private ClientA act;
	
	
	private MainMenuButton wolBtn;
	private MainMenuButton shutdownBtn;
	private MainMenuButton controlBtn;
	private MainMenuButton scheduleBtn;


	public ClientAView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		this.act = (ClientA)this.getContext();
		initBtns();

	}
	private void initBtns(){
		Display display = act.getWindowManager().getDefaultDisplay(); 
		int dWidth = display.getWidth();	//720;
		int dHeight = display.getHeight();

		
		wolBtn = new MainMenuButton(this, 240-40, 400, "WOL");
		shutdownBtn = new MainMenuButton(this, 480+40, 400, "Shutdowon");
		controlBtn = new MainMenuButton(this, 240-40, 700, "Control");
		scheduleBtn = new MainMenuButton(this, 480+40, 700, "Schedule");
	}
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		paint = new Paint();
		this.setBackgroundColor(0xFF000000);
	
		wolBtn.drawButton(canvas, paint);
		shutdownBtn.drawButton(canvas, paint);
		controlBtn.drawButton(canvas, paint);
		scheduleBtn.drawButton(canvas, paint);
	}
	
	
	public boolean onTouchEvent(MotionEvent event){
		
//		if(event.getAction() == MotionEvent.ACTION_DOWN){
//			Intent intent ;
//			if(wolBtn.contains((int)event.getX(), (int)event.getY())){
//				act.popupWol();	
//				
//			}else if(shutdownBtn.contains((int)event.getX(), (int)event.getY())){
//				intent	= new Intent(act, ShutdownNaviActivity.class);
//				act.startActivity(intent);
//				
//			}else if(controlBtn.contains((int)event.getX(), (int)event.getY())){
//				intent	= new Intent(act, MovieControllerActivity.class);
//				act.startActivity(intent);
//				
//			}else if(scheduleBtn.contains((int)event.getX(), (int)event.getY())){
//				intent	= new Intent(act, ScheduleListActivity.class);
//				act.startActivity(intent);
//				
//			}
//		}else if(event.getAction() == MotionEvent.ACTION_UP){
//			
//			invalidate();	
//			
//		}else if(event.getAction() == MotionEvent.ACTION_MOVE){
//			
//			
//			invalidate();	
//			
//		}
		
		return true;
	}
}
