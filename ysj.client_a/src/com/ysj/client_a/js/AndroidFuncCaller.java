package com.ysj.client_a.js;

import com.ysj.client_a.MovieControllerActivity;
import com.ysj.client_a.command.ClientSideCommand;
import com.ysj.client_a.msg.MessageManager;

import client_a_service.msg.common.MessageMaker;


import android.content.Context;
import android.content.Intent;
import android.sax.StartElementListener;
import android.util.Log;
import android.widget.Toast;

public class AndroidFuncCaller {
	private static final String TAG = "AndroidJS";
	private ClientSideCommand clientSideCommand;
	private MessageManager messageManager;
	private Context context;
	
	public AndroidFuncCaller(MessageManager messageManager, Context context){
		this.messageManager = messageManager;
		this.clientSideCommand = new ClientSideCommand();
		this.context = context;
	}
	public void runWol(/*final String arg*/){
		int reVal = clientSideCommand.runWol();
		Log.d(TAG, reVal+"");
		if(reVal == 0){
			messageManager.send(MessageMaker.repeatConnectTry());
		}	
	}
	
	public void shutdown(final int remainTime){
		messageManager.send(MessageMaker.shutdown(remainTime));
	}
	
	public void showControl(){
		Intent i = new Intent(context, MovieControllerActivity.class);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(i);
	}
}
