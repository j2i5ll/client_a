package com.ysj.client_a.js;

import client_a_service.connect.ClientSocket;
import android.util.Log;
import android.webkit.WebView;

public class WebJSCaller {

	private static final String TAG = "WebJS";
	private WebView webView;
	

	public WebJSCaller(WebView webView){
		this.webView = webView;
	}
	//<script language="text/javascript" src="file:///android_asset/js/runByAndroid.js" ></script>
	
	public void stateUpdate(){
		webView.post(new Runnable(){
			@Override
			public void run() {
				if(ClientSocket.isConnected){
					webView.loadUrl("javascript:stateOnline();");
				}else{
					webView.loadUrl("javascript:stateOffline();");
				}
			}
		});
	}

	public void stateOnline(){
		webView.post(new Runnable(){
			@Override
			public void run() {
				webView.loadUrl("javascript:stateOnline();");
			}
		});
	}
	public void stateOffline(){
		webView.post(new Runnable(){
			@Override
			public void run() {
				webView.loadUrl("javascript:window.android.stateOffline()");
			}
		});
	}
}
