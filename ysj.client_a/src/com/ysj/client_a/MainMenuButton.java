package com.ysj.client_a;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;
import android.view.View;


public class MainMenuButton {
	
	private int  borderColor;
	private String btnText;
	private int btnTextSize;

	private String ableText;
	private int ableTextSize;

	private Point centerPoint;
	
	private int width = 250;
	private int height = 170;
	private View parentView;
	
	private MainMenuButton(View view, String btnText){
		this.btnText = btnText;
		this.ableTextSize=20;
		this.btnTextSize=40;
		this.parentView = view;
		setEnable();
	}
	public MainMenuButton(View view, Point centerPoint, String btnText){
		this(view, btnText);
		this.centerPoint = centerPoint;

	}
	public MainMenuButton(View view, int x, int y, String btnText){
		this(view, btnText);
		this.centerPoint = new Point(x, y);
		

	}
	public void setEnable(){
		borderColor = Color.argb(255, 0, 255, 0);
		this.ableText = "E N A B L E";
		parentView.invalidate();
	}
	public void setDisable(){
		borderColor = Color.argb(255, 255, 0, 0);
		this.ableText = "D I S A B L E";
		parentView.invalidate();
	}
	public boolean contains(int x, int y){
		Rect rect;
		rect = new Rect(centerPoint.x-width/2, centerPoint.y-height/2, centerPoint.x+width/2, centerPoint.y+height/2);
		
		return rect.contains(x, y);
		
	}
	public void drawButton(Canvas canvas, Paint paint){
		paint.setStyle(Style.STROKE);
		paint.setStrokeWidth(2);
		paint.setColor(borderColor);
		float[] path = new float[6*4];
		path[0] = centerPoint.x + width/2.0f;
		path[1] = centerPoint.y - height/2.0f;
		path[2] = centerPoint.x - width/4.0f;
		path[3] = centerPoint.y - height/2.0f;

		path[4] = centerPoint.x - width/4.0f;
		path[5] = centerPoint.y - height/2.0f;
		path[6] = centerPoint.x - width/2.0f;
		path[7] = centerPoint.y ;

		path[8] = centerPoint.x - width/2.0f;
		path[9] = centerPoint.y ;
		path[10] = centerPoint.x - width/2.0f;
		path[11] = centerPoint.y + height/2.0f;

		path[12] = centerPoint.x - width/2.0f;
		path[13] = centerPoint.y + height/2.0f;
		path[14] = centerPoint.x + width/2.0f;
		path[15] = centerPoint.y + height/2.0f;
		
		path[16] = centerPoint.x + width/2.0f;
		path[17] = centerPoint.y + height/2.0f;
		path[18] = centerPoint.x + width/2.0f;
		path[19] = centerPoint.y - height/2.0f;

		path[20] = centerPoint.x - width/2.0f;
		path[21] = centerPoint.y + height/3.0f;
		path[22] = centerPoint.x + width/2.0f;
		path[23] = centerPoint.y + height/3.0f;

		canvas.drawLines(path, paint);
		
		
		paint.setTextSize(btnTextSize);
		paint.setStyle(Style.FILL);
		//canvas.drawText(btnText, centerPoint.x-40, centerPoint.y+10, paint);
		canvas.drawText(btnText, centerPoint.x-(btnTextSize * btnText.length()/4), centerPoint.y+btnTextSize/5, paint);
		paint.setTextSize(ableTextSize);
		canvas.drawText(ableText, centerPoint.x-50, centerPoint.y+77, paint);
	}
}
