package com.ysj.client_a;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Point;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;


class Circle{
	Point point;
	int radius;
	public Circle(int x, int y, int radius){
		this.point = new Point(x, y);
		this.radius = radius;
	}
	public Circle(Point point, int radius){
		this.point = new Point(point.x, point.y);
		this.radius = radius;
	}
	public int getX(){
		return this.point.x;
	}
	public int getY(){
		return this.point.y;
	}
	public void setPoint(int x, int y){
		this.point.x=x;
		this.point.y=y;
	}
	public int getRadius(){
		return this.radius;
	}
	public boolean contains(int x, int y){
		int absX = Math.abs(point.x - x);
		int absY = Math.abs(point.y - y);
		int slopLenSq = absX * absX + absY * absY;
		int radiusSq = this.radius * this.radius;
		
		
		return slopLenSq <= radiusSq ? true : false;
	}
}
public class MovieControllerView extends View {

	private Paint mPaint;

	
	private boolean isDrawCircle=false;
	private MovieControllerActivity act ;

	private Point touchOnPoint;
	private Point nowTouchPoint;
	

	
	
	private int HOLD_POSITION_THRESHOLD = 20;

	
	private int UPPER_POSITION = 200;
	
	private Circle pauseCircle;
	private Circle invalidityCircle;
	


	
	Timer holdCounter;
	
	public MovieControllerView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		mPaint = new Paint();
		this.setBackgroundColor(0xFF000000);
		
		holdCounter = null;
		nowTouchPoint = new Point(0, 0);
		touchOnPoint = new Point(0, 0);

		this.act = (MovieControllerActivity)this.getContext();
		this.pauseCircle = new Circle(0, 0, 30);
		this.invalidityCircle = new Circle(0, 0, 80);

		if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
			setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		} 

	}
	
	
	
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		if(isDrawCircle){

			mPaint.setAntiAlias(true);
		
			
			mPaint.setStyle(Style.STROKE);
			mPaint.setStrokeWidth(2);
			mPaint.setColor(Color.argb(255, 255, 0, 0));
			mPaint.setPathEffect(null);
			canvas.drawCircle(pauseCircle.getX(), pauseCircle.getY(),pauseCircle.getRadius(), mPaint);
			

			mPaint.setStyle(Style.STROKE);
			mPaint.setStrokeWidth(3);
			mPaint.setColor(Color.argb(255, 255, 128, 64) );
			canvas.drawCircle(invalidityCircle.getX(), invalidityCircle.getY(),invalidityCircle.getRadius(), mPaint);
			
			
			
			drawBoundary(canvas, mPaint);

			mPaint.setStyle(Style.FILL);
			mPaint.setColor(Color.argb(200, 255, 255, 255));
			canvas.drawCircle(nowTouchPoint.x, nowTouchPoint.y, 20, mPaint);
			
			
			
			
		}	
	}
	void drawBoundary(Canvas canvas, Paint mPaint){
		int dx = 0;
		int dy = 0;

		int xlenOuter = 0;
		int ylenOuter = 0;
		int xlenInner = 0;
		int ylenInner = 0;
		
		int endX = 0;
		int endY = 0;
		int deg=70;
		int lineLen=1000;
		
		mPaint.setStyle(Style.STROKE);
		mPaint.setStrokeWidth(1);
		mPaint.setColor(Color.argb(255, 0, 255, 255) );
		mPaint.setPathEffect(new DashPathEffect(new float[] {10,10}, 1));

		dx = (int)(Math.cos(Math.toRadians(deg)) * invalidityCircle.radius);
		dy = (int)(Math.sin(Math.toRadians(deg)) * invalidityCircle.radius);
		endX = (int)(Math.cos(Math.toRadians(deg)) * (invalidityCircle.radius+lineLen));
		endY = (int)(Math.sin(Math.toRadians(deg)) * (invalidityCircle.radius+lineLen));

		
		canvas.drawLine(touchOnPoint.x + dx , touchOnPoint.y - dy, touchOnPoint.x + endX,  touchOnPoint.y - endY, mPaint);
		canvas.drawLine(touchOnPoint.x - dx, touchOnPoint.y - dy, touchOnPoint.x - endX,  touchOnPoint.y - endY, mPaint);
		canvas.drawLine(touchOnPoint.x - dx, touchOnPoint.y + dy, touchOnPoint.x - endX,  touchOnPoint.y + endY, mPaint);
		canvas.drawLine(touchOnPoint.x + dx, touchOnPoint.y + dy, touchOnPoint.x + endX,  touchOnPoint.y + endY, mPaint);

	}
	
	public boolean onTouchEvent(MotionEvent event){
		if(event.getAction() == MotionEvent.ACTION_DOWN){
			isDrawCircle=true;
			touchOnPoint = new Point((int)event.getX(), ((int)event.getY())-UPPER_POSITION);
			nowTouchPoint = new Point((int)event.getX(), ((int)event.getY())-UPPER_POSITION);
			
			pauseCircle.setPoint((int)event.getX(), (int)event.getY()-UPPER_POSITION);
			invalidityCircle.setPoint((int)event.getX(), (int)event.getY()-UPPER_POSITION);
			
						
			invalidate();	
		}else if(event.getAction() == MotionEvent.ACTION_UP){
			if(holdCounter != null){
				holdCounter.cancel();
				holdCounter = null;
			}
			
			controlling();
			
			isDrawCircle=false;			
			invalidate();	
			
		}else if(event.getAction() == MotionEvent.ACTION_MOVE){
			
			
			nowTouchPoint = new Point((int)event.getX(), ((int)event.getY())-UPPER_POSITION);
			
			if(!invalidityCircle.contains(nowTouchPoint.x, nowTouchPoint.y)
					&& holdCounter == null){	
				repeatControl();
			}
			
			invalidate();	
			
		}
		
		return true;
	}
	
	private void repeatControl(){
		
		holdCounter = new Timer();
		holdCounter.schedule(new TimerTask(){

			private int WAIT_COUNT=4;
			private int count=0;
			private boolean isFirst=true;
			private Point holdingPos;
			@Override
			public void run() {
				// TODO Auto-generated method stub
				if(isFirst){
			
					holdingPos = nowTouchPoint;
					isFirst=false;
					
				}else{
				
					if( Math.abs(holdingPos.x - nowTouchPoint.x) <= HOLD_POSITION_THRESHOLD 
							&& Math.abs(holdingPos.y - nowTouchPoint.y) <= HOLD_POSITION_THRESHOLD ){
						count++;
						if(count > WAIT_COUNT){
							act.controllMovie(touchOnPoint, nowTouchPoint);
						}
					}else{
						holdCounter.cancel();
						
						//why holdCounter do not set null?
						//prevent to other position's autoControl operation with before pushing signal
					}
				}
				
			}
			
		}, 200, 200);
	}
	
	private void controlling(){
		
		if(pauseCircle.contains(nowTouchPoint.x, nowTouchPoint.y)){
			act.puaseMovie();
		}else if(invalidityCircle.contains(nowTouchPoint.x, nowTouchPoint.y)){
			//to do nothing
		}else{
			act.controllMovie(touchOnPoint, nowTouchPoint);
		}
		
		nowTouchPoint = new Point(0, 0);
		touchOnPoint = new Point(0, 0);
	}
	

	

}
