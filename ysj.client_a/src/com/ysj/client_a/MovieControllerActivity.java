package com.ysj.client_a;








import client_a_service.msg.common.MessageMaker;

import com.ysj.client_a.msg.MessageManager;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Message;
import android.view.WindowManager;

public class MovieControllerActivity extends Activity{


	private MovieControllerView movieControllerView;
	private MessageManager messageManager;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    
	    movieControllerView = new MovieControllerView(this);
	    messageManager = MessageManager.getInstance();
	    
	    setContentView(movieControllerView);
	    // TODO Auto-generated method stub
	    this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	
	   
	}
	
	public void controllMovie(Point start, Point end){
		int dx;
		int dy;
		
		
		dx = Math.abs(end.x - start.x);
		dy = Math.abs(end.y - start.y);
		
		Message msg;
		if(dx > dy){	//left or right
			if(end.x - start.x > 0){		//right
				msg = MessageMaker.control("right");
				messageManager.send(msg);
				
			}else{							//left
				msg = MessageMaker.control("left");
				messageManager.send(msg);
			}
		}else{			//up or down
			if(end.y - start.y > 0){		//down
				msg = MessageMaker.control("down");
				messageManager.send(msg);
			}else{							//up
				msg = MessageMaker.control("up");
				messageManager.send(msg);
			}
		}
	}
	public void puaseMovie(){
		Message msg;
		msg = MessageMaker.control("center");
		messageManager.send(msg);
		
	}

}
