package com.ysj.client_a;


import java.net.HttpURLConnection;
import java.net.URL;

import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.util.Log;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import client_a_service.ClientAService;
import client_a_service.msg.common.MessageMaker;

import com.ysj.client_a.js.AndroidFuncCaller;

import com.ysj.client_a.msg.MessageManager;

public class ClientA extends Activity  {
	private static final String TAG = "ClientA";

	private WebView webView;
	private MessageManager messageManager;
	//private WebJS webJS;
	
	private ServiceConnection mConnection = new ServiceConnection(){

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			// TODO Auto-generated method stub
			Log.i(TAG, "onServiceConnected");
			
			
			//MessageManager init one time here.
			messageManager = MessageManager.getInstance();
			messageManager.init(webView);
			
			messageManager.setServiceMessenger( new Messenger(service) );
			if(messageManager.getServiceMessenger() != null){
				AndroidFuncCaller androidFuncCaller = new AndroidFuncCaller(messageManager, getApplicationContext());
			    webView.addJavascriptInterface(androidFuncCaller, "clientAJS");
			        
				Message msg = new Message();
				msg.what = 0;
				msg.obj = new Messenger(messageManager);
				try{
					messageManager.send(msg);
					Log.i(TAG, "send App's messageManager");
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}else{
				Log.e(TAG, "Activity Cannot send Messenger to service");
			}
			

		}
		@Override
		public void onServiceDisconnected(ComponentName name) {
			// TODO Auto-generated method stub
			
		}
	};
	
	
	@Override 
	public void onBackPressed(){
		if(webView.canGoBack()){
			webView.goBack();
		}else{
			finish();
		}
	}
	
    @SuppressLint("SetJavaScriptEnabled")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        
        
       
        
        webView = (WebView)findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setAllowFileAccessFromFileURLs(true);
        webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.setWebViewClient(new WebClient());
       
        webView.setWebChromeClient(new WebChromeClient());
        webView.loadUrl("file:///android_asset/html/index.html");
        
       
        
        startService();
        //webView.loadUrl("file:///android_asset/js/runByAndroid.js");
        
    }
    @Override 
    public void onDestroy(){
    	super.onDestroy();
    	unbindService(mConnection);
    }
    
    private void startService(){
    	Intent intent = new Intent(this, ClientAService.class);
    	bindService(intent,  mConnection, Context.BIND_AUTO_CREATE);
    }

    
    
    private class WebClient extends WebViewClient {
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
        
    	@Override
    	public void onPageFinished(WebView view, String url){
    		super.onPageFinished(view, url);
    		//webJS.stateUpdate();
    		Messenger messenger = new Messenger(messageManager);	//send to myself(activity)
			Message msg = MessageMaker.stateUpdate();
    		
    		try {
				messenger.send(msg);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		//webJS.stateUpdate();
			
			
    	}
    
    }
}