package com.ysj.client_a;

import java.io.BufferedReader;

import android.app.Activity;

interface ResponseListener{
	public int onResponseProcess(BufferedReader messageBuffer)throws Exception;
}
public abstract class CommunicationActivity extends Activity implements ResponseListener{

}
