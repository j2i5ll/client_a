package client_a_service.msg.common;

import android.os.Message;

public class MessageMaker {
	public static Message stateUpdate(){
		Message msg = new Message();
		msg.what = MessageStoAConstant.MSG_STATE_UPDATE;
		return msg;
	}
	
	public static Message repeatConnectTry(){
		Message msg = new Message();
		msg.what = MessageAtoSConstant.MSG_REPEAT_CONNECT_TRY;
		return msg;
	}
	public static Message shutdown(int remainTime){
		Message msg = new Message();
		msg.what = MessageAtoSConstant.MSG_SHUTDOWN;
		msg.arg1 = remainTime;
		return msg;
	} 
	public static Message control(String cmd){
		Message msg = new Message();
		msg.what = MessageAtoSConstant.MSG_CONTROL;
		msg.obj = cmd;
		return msg;
	}
}
