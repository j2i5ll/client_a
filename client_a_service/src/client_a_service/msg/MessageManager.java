package client_a_service.msg;


import client_a_service.connect.ClientSocket;
import client_a_service.msg.common.MessageAtoSConstant;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;



//service MessageManager
public class MessageManager extends Handler{
	private Messenger appMessenger=null;
	
	private static final String TAG = "MessageManager";
	private ClientSocket clientSocket;
	public MessageManager(ClientSocket clientSocket){
		this.clientSocket = clientSocket;
	}
	
	@Override
	public void handleMessage(Message msg){
		
		String cmd="";
		Log.d(TAG, "RECV : "+msg.what);
		switch(msg.what){
			case 0 :
				appMessenger = (Messenger)msg.obj;
				Log.i(TAG, "Receive app's messenger");
				break;
			case MessageAtoSConstant.MSG_REPEAT_CONNECT_TRY:
				clientSocket.connectRepeat(3000, 10);
				break;
			case MessageAtoSConstant.MSG_SHUTDOWN:
				cmd = "shutdown@"+msg.arg1;
				clientSocket.send(cmd.getBytes());
				break;
			case MessageAtoSConstant.MSG_CONTROL:
				cmd = "control@"+msg.obj;
				clientSocket.send(cmd.getBytes());
				break;
			default:
				break;
		}
	}
	public void send(Message msg){
		if(appMessenger!=null){
			
			try {
				appMessenger.send(msg);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}
}
