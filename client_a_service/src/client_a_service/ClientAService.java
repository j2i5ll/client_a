package client_a_service;

import client_a_service.msg.MessageManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

public class ClientAService extends Service{
	
	
	private static final String TAG = "ClientAService";
	private static final int NOTI_ID = 1;
	
	private Notification notification;
	private PendingIntent pendingIntent;
	private ServiceInitiater serverInitiater;

	
	
	@Override
	public void onCreate(){
		super.onCreate();
		Log.i(TAG, "constructor");
		this.serverInitiater = new ServiceInitiater();
		
		notification = new Notification(android.R.drawable.ic_dialog_info, "ClientA Start", System.currentTimeMillis());
		Intent notificationIntent = new Intent(this, ClientAService.class);
		pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
		notification.setLatestEventInfo(this, "ClientA", "Message of ClientA", pendingIntent);
		//notification.setLatestEventInfo(this, "TTT", "AAA", pendingIntent);
		startForeground(NOTI_ID, notification);
		
		
		
	}
	public ServiceInitiater getServiceInitiater(){
		return this.serverInitiater;
	}
	

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return new Messenger(this.serverInitiater.getMessageManager()).getBinder();
	}
	
	@Override
	public boolean onUnbind(Intent intent){
		super.onUnbind(intent);
		
		return true;
	}
	
	@Override
	public void onRebind(Intent intent){
		
	}
	
	@Override
	public void onDestroy(){
		serverInitiater.terminate();
		stopForeground(true);
	}

}
