package client_a_service.connect;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import client_a_service.msg.MessageManager;
import client_a_service.msg.common.MessageMaker;
import android.util.Log;


public class ClientSocket extends Thread{
	private static final String TAG = "ClientSocket";

	private static final int QUEUE_SIZE = 50;
	public static boolean isConnected = false;
	//private WebView webView;

	private String host;
	private int port;
	private Socket socket;
	private SocketAddress socketAddress;
	
	private OutputStream os;
	private InputStream is;
	private BlockingQueue<byte[]> writeData;
	private WriteThread wt;
	private boolean isRun;

	private int reconnectCount=-1;
	private int reconnectInterval=0;
	private int nowReconnectCount=0;
	private MessageManager messageManager;
	
	public ClientSocket(){
		this("k2u4yt.iptime.org", 30501);
		//this("165.132.221.43", 8000);
		
	}
	
	public ClientSocket(String host, int port){
		this.host = host;
		this.port = port;
		this.socketAddress = null;
		this.writeData = new ArrayBlockingQueue<byte[]>(QUEUE_SIZE);
		this.isRun=true;
		initRetry();
	}
	public void setMessageManager(MessageManager messageManager){
		this.messageManager = messageManager;
		
	}
	
	public void terminate(){
		isRun=false;
		
		try {
			if(wt!=null && wt.getState() == Thread.State.RUNNABLE){
				byte[] terminateMsg = new byte[2];
				terminateMsg[0]=(byte) 0xde;
				terminateMsg[1]=(byte) 0xad;
				writeData.put(terminateMsg);
				Log.i(TAG, "send terminate message");
			}
			
			if(this.getState() == Thread.State.WAITING){
				initRetry();
				wakeup();
				Log.i(TAG, "terminate main thread");
			}
			
			socket.shutdownInput();
			socket.shutdownOutput();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	private boolean wakeup(){
		if(this.getState() == Thread.State.WAITING){
			synchronized (this) {
				this.notify();
			}
			return true;
		}else{
			return false;
		}
		
	}
	
	public void connectRepeat(int reconnectInterval, int reconnectCount){
		
		this.reconnectCount=reconnectCount;
		this.reconnectInterval = reconnectInterval;
		wakeup();
	}
	
	@Override
	public synchronized void run(){
	
		while(isRun){		
			Log.i(TAG, "Start Connect Thread");
			
			try {				//try first connect once
				if(socket!=null){
					socket.close();
					socket=null;
				}
				socket = new Socket();
				this.socketAddress = new InetSocketAddress(host, port);
				socket.connect(socketAddress, 2000);
			}catch(ConnectException | SocketTimeoutException ex){
				ex.printStackTrace();
				try {
					nowReconnectCount++;
					if(nowReconnectCount >= reconnectCount){
						Log.i(TAG, "in wait");
						this.wait();			//if fail , wait
						Log.i(TAG, "wakeup!");
					}else{
						Thread.sleep(reconnectInterval);
						Log.i(TAG, "retry : "+nowReconnectCount);
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				continue;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				continue;
			}	
			if(nowReconnectCount!=0){	// it means wol executed --> already page loaded
				//webJS.stateUpdate();
				messageManager.send(MessageMaker.stateUpdate());
			}
			Log.i(TAG, "connect success");
			initRetry();
			isConnected = true;
			
			afterConnect();
			
		}
		
		try{
			if(socket!=null){
				socket.close();
				socket=null;
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	private void initRetry(){
		nowReconnectCount = 0;
		reconnectCount = 0;
		reconnectInterval = 0;
	}
	private void startWrite(){
		try {
			this.os=socket.getOutputStream();
			wt = new WriteThread();
			wt.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void startRead(){
		//receive work
		int len=0;
		byte[] readBuf=new byte[512];
		try{
			this.is = socket.getInputStream();
			Log.i(TAG, "start read thread");
			while((len = is.read(readBuf)) > 0){

			}
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			Log.i(TAG, "close input stream");
			try{
				if(is!=null){
					is.close();
					is=null;
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
	}
	private void afterConnect(){
		startWrite();
		startRead();
	}
	public void send(byte[] buff){
		try {
			writeData.put(buff);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	private class WriteThread extends Thread{

		@Override
		public void run(){
			Log.i(TAG, "start write thread");
			byte[] buff=null;
			while(true){
				try {
					
					buff = writeData.take();
					if(isTerminateMsg(buff)){
						break;
					}
					os.write(buff);
					os.flush();

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			try{
				Log.i(TAG, "close output stream");
				if(os!=null){
					os.close();
					os=null;
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}
		private boolean isTerminateMsg(byte[] buff){
			if(buff.length==2 &&
				buff[0]==0xde &&
				buff[1]==0xad ){
				Log.i(TAG, "receive terminate message");
				return true;
			}
			return false;

		}
	}
}
