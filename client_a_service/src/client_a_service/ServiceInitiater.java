package client_a_service;


import client_a_service.connect.ClientSocket;
import client_a_service.msg.MessageManager;
import android.util.Log;
import android.webkit.WebView;

public class ServiceInitiater {
	private static final String TAG = "ServerConnectionManager";

	private ClientSocket clientSocket;
	private MessageManager messageManager;
	
	public ServiceInitiater(){
		Log.i(TAG, "constructor");
		//this.clientSocket = new ClientSocket("dist.obigo.com", 9091);
		
		this.clientSocket = new ClientSocket();
		this.clientSocket.start();
		this.messageManager = new MessageManager(clientSocket);
		this.clientSocket.setMessageManager(this.messageManager);	
	}
	public MessageManager getMessageManager(){
		return this.messageManager;
	}
	public void terminate(){
		this.clientSocket.terminate();
	}

}
